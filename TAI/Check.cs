﻿using Ionic.Zip;
using KAutoHelper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Tesseract;
using xNet;

namespace TAI
{
    public class ADB
    {
        private string deviceID;
        private string pathForder_Tesseract;
        public ADB(string deviceID, string pathForder_Tesseract)
        {
            this.deviceID = deviceID;
            this.pathForder_Tesseract = pathForder_Tesseract;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceID"></param>
        /// <returns>path of xml file</returns>
        public static string GetXML(string deviceID)
        {
            string fileName = $"window_dump_{ deviceID}.xml";
            Computer.RunCMD($"adb -s {deviceID} shell uiautomator dump -a");
            Computer.RunCMD($"adb -s {deviceID} pull /sdcard/window_dump.xml {fileName}");
            return fileName;
        }
        /// <summary>
        /// Require install "Tesseract"
        /// </summary>
        /// <param name="bitmap"></param>
        /// <returns></returns>
        public string GetTextFromImage(System.Drawing.Bitmap bitmap)
        {
            string text = "";

            if (bitmap != null)
            {
                try
                {
                    TesseractEngine tesseractEngine = new TesseractEngine(pathForder_Tesseract, "vie", EngineMode.Default);
                    tesseractEngine.DefaultPageSegMode = PageSegMode.SparseText;
                    Page page = tesseractEngine.Process(bitmap);
                    text = page.GetText();
                }

                catch { }
            }


            return text;
        }
        public string GetScreentext(ref Bitmap screen)
        {
            screen =  ScreenShoot(false);
            if (screen == null)
                return "error_screenshoot";
            return GetTextFromImage(screen);
        }
        public string Click(string[] subText, Bitmap bitmap)
        {
            string result = "0";
            foreach (var text in subText)
            {
                Point p = Computer.GetPosition(bitmap, text, pathForder_Tesseract);
                if (p != Point.Empty)
                {
                    KAutoHelper.ADBHelper.Tap(deviceID, p.X, p.Y);
                    result = "1";
                    break;
                }
            }
            
            return result;
        }
        public string Click(string[] subText)
        {
            string result = "0";
            Bitmap screen = ScreenShoot(false);
            foreach (var text in subText)
            {
                Point p = Computer.GetPosition(screen, text, pathForder_Tesseract);
                if (p != Point.Empty)
                {
                    KAutoHelper.ADBHelper.Tap(deviceID, p.X, p.Y);
                    result = "1";
                    break;
                }
            }
            screen.Dispose();
            return result;
        }
        public Bitmap ScreenShoot(bool isDeleteImageAfterCapture = true, string fileName = "screenShoot")
        {

            //string screenShotCount = deviceID;
            //try
            //{
            //    screenShotCount = deviceID.Split(new char[]
            //    {
            //':'
            //      })[1];
            //     }
            //catch
            //         {
            //         }

            string nameToSave = fileName + deviceID + ".png";
            for (; ; )
            {
                bool flag3 = File.Exists(nameToSave);
                if (!flag3)
                {
                    break;
                }
                try
                {
                    File.Delete(nameToSave);
                    break;
                }
                catch (Exception ex)
                {
                    break;
                }
            }
            string Current = Directory.GetCurrentDirectory() + "\\" + nameToSave;
            string CurrentPath = Directory.GetCurrentDirectory().Replace(@"\\", @"\");
            CurrentPath = "\"" + CurrentPath + "\"";

            string cmdCommand1 = string.Format("adb -s {0} exec-out screencap -p > {1}", deviceID, nameToSave);
            string result1 = Computer.RunCMD(cmdCommand1);

            Bitmap result = null;
            try
            {
                using (Bitmap bitmap = new Bitmap(Current)) // Thỉnh thoảng sẽ bị lỗi khi lưu file bị 0 byte
                {
                    result = new Bitmap(bitmap);
                }
            }
            catch
            {

            }

            if (isDeleteImageAfterCapture)
            {
                try
                {
                    File.Delete(nameToSave);
                }
                catch
                {
                }
            }
            return result;
        }
        public class FacebookLite
        {
            public class Click
            {
                public static string TextBox(string deviceID)
                {
                    string status = "0";
                    string fileName = GetXML(deviceID);
                    string[] textBoxClass = { "android.widget.MultiAutoCompleteTextView", "android.widget.EditText" };
                    //android.widget.EditText[^<]+bounds=\\"\[(?<X1>\d+),(?<Y1>\d+)\]\[(?<X2>\d+),(?<Y2>\d+)
                    foreach (var tb in textBoxClass)
                    {
                        string pattern = $"{tb}[^<]+bounds=\\\"\\[(?<X1>\\d+),(?<Y1>\\d+)\\]\\[(?<X2>\\d+),(?<Y2>\\d+)";

                        Regex rg2 = new Regex(pattern);
                        string textValue = File.ReadAllText(fileName);
                        var match = rg2.Match(textValue);
                            if (match.Success)
                            {
                                int X1, Y1, X2, Y2, X, Y;
                        X1 = int.Parse(match.Groups["X1"].ToString());
                        X2 = int.Parse(match.Groups["X2"].ToString());
                        Y1 = int.Parse(match.Groups["Y1"].ToString());
                        Y2 = int.Parse(match.Groups["Y2"].ToString());
                        X = (X1 + X2) / 2;
                                Y = (Y1 + Y2) / 2;
                                KAutoHelper.ADBHelper.Tap(deviceID, X, Y);
                                status = "1";
                            break;
                            }
                    }
                    
                    return status;
                }
            }
           
        }
        public void Set_ADBKeyboard()
        {
            Computer.RunCMD($"adb -s {deviceID} shell ime set com.android.adbkeyboard/.AdbIME");
        }
        
      
    }
    public class Check
    {
        
        public static bool Empty(string str)
        {
            if (string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str)) return true;
            else return false;
        }
        public static bool Contains(string str,string[] subStringArray, bool isMatchCase = false)
        {
            if (isMatchCase)
            {
                str = str.ToLower();
                for (int i = 0; i < subStringArray.Count(); i++)
                    subStringArray[i] = subStringArray[i].ToLower();
            }
            foreach (var item in subStringArray)
            {
                if (str.Contains(item))
                    return true;
            }
            return false;
        }
        public static int HowManyCharSumInString(char charr, string stringg)
        {
            return stringg.Count(f => f == charr);
        }
        public static bool IsVietNameName(string name) {
            bool is_VN = false;
            char[] charArr = {'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j',
            'k','l','z','x','c','v','b','n','m'};
            //Kiểm tra nhanh (có unicode => VN)
            if (name.Contains("ấ") || name.Contains("ầ") || name.Contains("à") || name.Contains("ắ") || name.Contains("á") || name.Contains("ă") || name.Contains("ả") || name.Contains("ạ")
                || name.Contains("ễ") || name.Contains("ể") || name.Contains("ệ") || name.Contains("ê")
                || name.Contains("ì") || name.Contains("ị")) // để giúp tool check nhanh hơn ta sẽ check xem tên có 1 số dấu đặc trưng của tên tiếng việt , nếu ko có mới kiểm tra sâu hơn
            {
                is_VN = true;
            }
            else
            {
                //Kiểm tra kĩ hơn
                string[] wordArr = File.ReadAllText(@"MyData/isnot-vietnam-name.txt").Trim().Split('\n');

                bool is_name_vietnam = true;
                for (int i = 0; i < name.Length; i++)
                {
                    for (int k = 0; k < charArr.Count(); k++)
                    {
                        if (name[i] == charArr[k])
                        {
                            for (int o = 0; o < wordArr.Count(); o++)
                            {
                                if (name.Contains(wordArr[o].Trim()))
                                {
                                    is_VN = false;
                                    is_name_vietnam = false;
                                    break;
                                }
                            }
                            if (is_name_vietnam) is_VN = true;

                            break;
                        }
                        if (is_VN)
                        {
                            break;
                        }
                    }

                }
            }
            return is_VN;
        }
        
    }
    public class Computer
    {
        /// <summary>
        ///
        /// </summary>
        /// <returns>Tuple(filePath,fileContent)</returns>
        public static Tuple<string,string> ShowDialogFile()
        {
            Tuple<string, string> result = null;
            var fileContent = string.Empty;
            var filePath = string.Empty;
            using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                    }
                    result = new Tuple<string, string>(filePath, fileContent);
                }
            }
            return result;
        }
        public static void ZipFile(string pathFile,string savePath,string password = "")
        {
            //Hiện không hoạt động


            string nameFile = "";
            Regex regex = new Regex("(?<nameFile>[A-Za-z]+).[A-z]+$");
            nameFile = regex.Match(pathFile).Groups["nameFile"].ToString();
            if (pathFile.Contains("/"))
            using (ZipFile zip = new ZipFile())
            {
                if(password!="")
                    zip.Password = password;
                zip.AddFile(pathFile);
                    if (savePath == "")
                    {
                        savePath = AppDomain.CurrentDomain.BaseDirectory;
                        zip.Save($@"{savePath}{nameFile}.zip");

                    }
                    else
                        zip.Save($@"{savePath}/{nameFile}.zip");
            }
        }
        public static void ZipForder(string pathForder, string savePath, string nameForder, string password = "")
        {

            using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
            {
                if (password != "")
                    zip.Password = password;
                zip.AddDirectory(pathForder);
                if (savePath == "")
                {
                    savePath = AppDomain.CurrentDomain.BaseDirectory;
                    zip.Save($@"{savePath}{nameForder}.zip");

                }
                else
                    zip.Save($@"{savePath}/{nameForder}.zip");
            }
        }
        public static void UnZipForder(string pathZip, string savePath)
        {

            using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(pathZip))
            {
                zip.ExtractAll(savePath, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
            }
        }
        public static string Click(string subImagePath, IntPtr intPtr, int times = 1)
        {
            FileInfo[] allPathFile_Forder = new DirectoryInfo(subImagePath).GetFiles();
            for (int i = 1; i <= times; i++)
            {
                AutoControl.BringToFront(intPtr);
                Thread.Sleep(500);
                var screenC = CaptureHelper.CaptureScreen();
                foreach (var SubImage in allPathFile_Forder)
                {
                    var subIamgeC = ImageScanOpenCV.GetImage(SubImage.FullName);
                    var resBitmap = ImageScanOpenCV.FindOutPoint((Bitmap)screenC, subIamgeC);
                    if (resBitmap != null)
                    {
                        AutoControl.MouseClick(resBitmap.Value.X + subIamgeC.Size.Width / 2, resBitmap.Value.Y + subIamgeC.Size.Height / 2, EMouseKey.LEFT);
                        return "1";
                    }

                }
                if (times > 5)
                {
                    if ("break" == HandleException_Click(subImagePath, intPtr))
                    {
                        break;
                    }
                }
            }


            return "0";


        }
        /// <summary>
        /// Require install "Tesseract"
        /// </summary>
        /// <param name="text"></param>
        /// <param name="pathForder_Tesseract"></param>
        /// <returns></returns>
        public static string Click(string text, string pathForder_Tesseract)
        {
            Point p = Point.Empty;
            while(p.IsEmpty)
            {
                using(Bitmap bitmap = (Bitmap)CaptureHelper.CaptureScreen())
                {
                    p = GetPosition(bitmap, text, pathForder_Tesseract);
                }
                
            }
            AutoControl.MouseClick(p.X, p.Y, EMouseKey.LEFT);
            return "1";
        }
        public static System.Drawing.Bitmap Crop(System.Drawing.Bitmap bm, Rectangle rect)
        {

            System.Drawing.Bitmap newBitmap = new System.Drawing.Bitmap(rect.Width, rect.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            newBitmap.MakeTransparent();
            if (bm != null)
            {
                using (Graphics g = Graphics.FromImage(newBitmap))
                {
                    bm.SetResolution(g.DpiX, g.DpiY);
                    g.DrawImage(bm, 0, 0, rect, GraphicsUnit.Pixel);
                }
            }
            return newBitmap;
        }
        public static string See(string subImagePath, IntPtr intPtr, int times = 1)
        {
            FileInfo[] allPathFile_Forder = new DirectoryInfo(subImagePath).GetFiles();
            for (int i = 1; i <= times; i++)
            {
                AutoControl.BringToFront(intPtr);
                Thread.Sleep(500);
                var screenC = CaptureHelper.CaptureScreen();
                foreach (var SubImage in allPathFile_Forder)
                {
                    var subIamgeC = ImageScanOpenCV.GetImage(SubImage.FullName);
                    var resBitmap = ImageScanOpenCV.FindOutPoint((Bitmap)screenC, subIamgeC);
                    if (resBitmap != null)
                    {
                        return "1";
                    }

                }
                if (times > 5)
                {
                    HandleException_Click(subImagePath, intPtr);
                }
            }


            return "0";
        }
        public static string HandleException_Click(string pathImage, IntPtr intPtr)
        {
            string result = "";
            if (pathImage == "Images//Computer//LDPlayer//Model")
            {
                Click("Images//Computer//LDPlayer//icon-settings", intPtr, 1);
            }
            else if (pathImage == "Images//Computer//LDPlayer//Create-Emei")
            {
                Click("Images//Computer//LDPlayer//Model", intPtr, 1);

            }
            else if (
                pathImage == "Images//Computer//LDPlayer//icon-settings"
                || pathImage == "Images//Computer//LDPlayer//Model"
                )
            {
                if ("1" == See("Images//Computer//LDPlayer//Create-Emei", intPtr, 1))
                {
                    result = "break";
                }
            }

            return result;

        }
        public static string RunCMD(string cmd)
        {
            Process cmdProcess;
            cmdProcess = new Process();
            cmdProcess.StartInfo.FileName = "cmd.exe";
            cmdProcess.StartInfo.Arguments = "/c " + cmd;
            cmdProcess.StartInfo.ErrorDialog = false;
            cmdProcess.StartInfo.RedirectStandardError = true;
            cmdProcess.StartInfo.RedirectStandardOutput = true;
            cmdProcess.StartInfo.UseShellExecute = false;
            cmdProcess.StartInfo.CreateNoWindow = true;
            cmdProcess.Start();
            string output = "";
            string outputError = "";
            // You can pass any delegate that matches the appropriate 
            // signature to ErrorDataReceived and OutputDataReceived
            cmdProcess.ErrorDataReceived += (sender, errorLine) => { if (errorLine.Data != null) outputError += errorLine.Data; };
            cmdProcess.OutputDataReceived += (sender, outputLine) => { if (outputLine.Data != null) output += outputLine.Data; };
            cmdProcess.BeginErrorReadLine();
            cmdProcess.BeginOutputReadLine();

            cmdProcess.WaitForExit(1000 * 10);
            return output;
        }
        /// <summary>
        /// Bắt buộc phải cài Newtonsoft.Json 13.0.1
        /// </summary>
        /// <param name="url"></param>
        /// <returns>
        ///  ===== If success it will return =================
        ///  JObject
        ///  ===== If have error will return strings =================
        ///  "Error: Bảo trì" , "Error: Lỗi không xác định" , "Error: Time out 60s";
        /// </returns>

        public static dynamic HttpRequest(string url, bool getJSON = true)
        {
            using (HttpRequest http = new HttpRequest())
            {
                string html = "-1";
                dynamic myData = null;
                var startTime = DateTime.Now;
                while (html == "-1")
                {
                    if ((DateTime.Now - startTime).TotalMinutes == 1)
                    {
                        throw new Exception("Error: Time out 60s");
                    }
                    http.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36";

                    try
                    {
                        html = http.Get(url).ToString();
                        if (html == "")
                            continue;
                    }
                    catch (Exception e)
                    {
                        if (e.Message.Contains("Не удалось соединиться с HTTP-сервером"))
                        {
                            throw new Exception("Error: Lỗi mạng");
                        }
                        else if (e.Message.Contains("Ошибка на стороне клиента"))
                        {
                            throw new Exception("Error: Lỗi phía máy khách");
                        }
                        else
                        {
                            throw new Exception("Error: Lỗi không xác định");
                        }
                    }

                    if (!getJSON)
                        return html;

                    try
                    {
                        myData = JObject.Parse(html);
                    }
                    catch (Exception e)
                    {
                        if (html.ToLower().Contains("bảo trì"))
                        {
                            throw new Exception("Error: Bảo trì");
                        }
                        else if (myData == null)
                        {
                            continue;
                        }
                        else
                        {
                            throw new Exception("Error: Lỗi không xác định");
                        }
                    }
                }
                return myData;
            }
        }
        public static Bitmap DownloadImage(string url, string nameFile = "imageDownload")
        {
            Bitmap result = null;
            using (var client = new System.Net.WebClient())
            {
                string path = nameFile + ".png";
                client.DownloadFile(url, nameFile + ".png");
                using (Bitmap bm = new Bitmap(path))
                {
                    result = new Bitmap(bm);
                }
            }

            return result;
        }
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        public static string GetActiveWindowTitle()
        {
            const int nChars = 256;
            StringBuilder Buff = new StringBuilder(nChars);
            IntPtr handle = GetForegroundWindow();

            if (GetWindowText(handle, Buff, nChars) > 0)
            {
                return Buff.ToString();
            }
            return null;
        }
        public static List<Process> GetAllProgress()
        {
            List<Process> result = new List<Process>();
            Process[] AllProcesses = Process.GetProcesses();
            foreach (var process in AllProcesses)
            {
                if (process.MainWindowTitle != "")
                {
                    result.Add(process);


                }
            }
            return result;
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
        public static string GetSerialNumber()
        {
            string hdSN = System.String.Empty;
            ManagementObjectSearcher moSearcher = new System.Management.ManagementObjectSearcher("select * from Win32_DiskDrive");
            foreach (System.Management.ManagementObject wmi_HDD in moSearcher.Get())
            {
                hdSN = wmi_HDD["SerialNumber"].ToString();
            }
            return hdSN;
        }
        [DllImport("user32.dll")]
        internal static extern IntPtr SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        internal static extern bool ShowWindow(IntPtr hWnd, int nCmdShow); //ShowWindow needs an IntPtr
        /// <summary>
        /// <para>SW = 1 : Activates and displays a window. If the window is minimized or maximized, the system restores it to its original size and position. An application should specify this flag when displaying the window for the first time.</para>
        /// <para>SW = 3 : Activates the window and displays it as a maximized window.</para>
        /// <para>SW = 5 : Activates the window and displays it in its current size and position.</para>
        /// </summary>
        /// <param name="title"></param>
        public static void FocusWindow(string title, int SW = 5)
        {
            IntPtr hWnd; //change this to IntPtr
            Process[] processRunning = Process.GetProcesses();
           
            /*
            
             */
            foreach (Process pr in processRunning)
            {

                if (pr.MainWindowTitle == title)
                {
                    hWnd = pr.MainWindowHandle; //use it as IntPtr not int
                    ShowWindow(hWnd, SW);
                    SetForegroundWindow(hWnd); //set to topmost
                }
            }
        }
        public static Bitmap GrayScale(Bitmap bitmap)
        {
            
            Bitmap d;
            int x, y;

            // Loop through the images pixels to reset color.
            for (x = 0; x < bitmap.Width; x++)
            {
                for (y = 0; y < bitmap.Height; y++)
                {
                    Color pixelColor = bitmap.GetPixel(x, y);
                    Color newColor = Color.FromArgb(pixelColor.R, 0, 0);
                    bitmap.SetPixel(x, y, newColor); // Now greyscale
                }
            }
            d = bitmap;   // d is grayscale version of c  
            return d;
        }
        public static string GetTextFromImage(System.Drawing.Bitmap bitmap)
        {
            string text = "";

            if (bitmap != null)
            {
                try
                {
                    TesseractEngine tesseractEngine = new TesseractEngine("tessdata_fast-master", "vie", EngineMode.Default);
                    tesseractEngine.DefaultPageSegMode = PageSegMode.SparseText;
                    Page page = tesseractEngine.Process(bitmap);
                    text = page.GetText();
                }

                catch { }
            }


            return text;
        }
        public static Point GetPosition(System.Drawing.Bitmap bitmap, string subText,string pathForder_Tesseract)
        {
            
            PageIteratorLevel myLevel = PageIteratorLevel.Word;
            Point p = new Point();
            if (bitmap == null)
            { return p; }
            TesseractEngine engine = new TesseractEngine(pathForder_Tesseract, "vie", EngineMode.Default);
            p = Point.Empty;
            string[] strArr = subText.Split(' ');
            ResultIterator iterTemp = null;
            var page = engine.Process(bitmap, PageSegMode.Auto);

            int X1 = 0;
            int Y1 = 0;
            int X2 = 0;
            int Y2 = 0;
            using (var iter = page.GetIterator())
            {
                iter.Begin();
                iterTemp = iter;
                for (int i = 0; i < strArr.Length; i++)
                {

                    int indexTemp = 0;
                    do
                    {

                        if (iterTemp.TryGetBoundingBox(myLevel, out var rect))
                        {
                            var curText = iter.GetText(myLevel);
                            if (curText == strArr[indexTemp]) //Here is your text
                            {
                                indexTemp++;
                                if (X1 == 0) //Lấy ra X,Y cả từ đầu
                                {
                                    X1 = rect.X1;
                                    Y1 = rect.Y1;
                                    //Nếu chỉ tìm 1 từ thì ta lấy luôn x2,y2
                                    if (strArr.Count() == 1)
                                    {
                                        X2 = rect.X2;
                                        Y2 = rect.Y2;
                                    }
                                }
                                else //  Lấy ra X,Y cả từ cuối
                                {
                                    X2 = rect.X2;
                                    Y2 = rect.Y2;
                                }
                                if (indexTemp == strArr.Length) // Đã tìm thấy hợp lệ đoạn str
                                {
                                    p.X = (X1 + X2) / 2;
                                    p.Y = (Y1 + Y2) / 2;
                                    break;
                                }
                            }
                            else if (indexTemp > 0) // Nếu indexTemp này mà >0, mà curText == strArr[indexTemp] đúng => cụm từ ở dòng này không hợp lệ, ta kiểm tra dòng tiếp theo
                            {
                                X1 = Y1 = X2 = Y2 = 0;
                                iterTemp = iter; // Lưu lại vị trí đã loop
                                break;
                            }
                        }
                    } while (iter.Next(myLevel));
                    if (p.X != 0) // Đã tìm thấy hợp lệ đoạn str
                    {
                        break;
                    }
                }

            }
            return p;
        }
        public static void MoveDirectory(string strSourceDir, string strDestDir)
        {
            if (Directory.Exists(strSourceDir))
            {
                if (Directory.GetDirectoryRoot(strSou­rceDir) == Directory.GetDirectoryRoot(strDest­Dir))
                {
                    while (true)
                    {
                        try
                        {
                            Directory.Move(strSourceDir, strDestDir);
                            break;
                        }
                        catch (Exception ex)
                        {
                            Thread.Sleep(1000);

                            if (!ex.Message.Contains("is denied"))
                            {

                            }
                        }

                    }
                }
                else
                {
                    try
                    {
                        CopyDirectory(strSourceDir, strDestDir);
                        Directory.Delete(strSo­urceDir, true);
                    }
                    catch (Exception subEx)
                    {
                        throw subEx;
                    }
                }
            }
        }
        public static void MoveDirectory(string strSourceDir, string strDestDir, string newNameDirectory)
        {
            if (Directory.Exists(strSourceDir))
            {
                try
                {
                    CopyDirectory(strSourceDir, strDestDir, newNameDirectory);
                    Directory.Delete(strSo­urceDir, true);
                }
                catch (Exception subEx)
                {
                    throw subEx;
                }
            }
        }
        private static void CopyDirectory(string diSourceDir, string diDestDir)
        {
            var sourceDir = new DirectoryInfo(diSourceDir);
            var destPath = diDestDir + "//" + sourceDir.Name;
            if (Directory.Exists(destPath) == false)
            {
                Directory.CreateDirectory(destPath);
            }
            var destDir = new DirectoryInfo(destPath);

            FileInfo[] fiSrcFiles = sourceDir.GetFiles();
            foreach (FileInfo fiSrcFile in fiSrcFiles)
            {
                fiSrcFile.CopyTo(Path.Combine(destDir.FullName, fiSrcFile.Name));
            }
            DirectoryInfo[] diSrcDirectories = sourceDir.GetDirectories();
            foreach (DirectoryInfo diSrcDirectory in diSrcDirectories)
            {
                CopyDirectory(diSrcDirectory.FullName, new DirectoryInfo(Path.Combine(destDir.FullName, diSrcDirectory.Name)).FullName);
            }
        }
        private static void CopyDirectory(string diSourceDir, string diDestDir, string newNameDirectory)
        {
            var sourceDir = new DirectoryInfo(diSourceDir);
            var destPath = diDestDir + "//" + newNameDirectory;
            if (Directory.Exists(destPath) == false)
            {
                Directory.CreateDirectory(destPath);
            }
            var destDir = new DirectoryInfo(destPath);

            FileInfo[] fiSrcFiles = sourceDir.GetFiles();
            foreach (FileInfo fiSrcFile in fiSrcFiles)
            {
                fiSrcFile.CopyTo(Path.Combine(destDir.FullName, fiSrcFile.Name));
            }
            DirectoryInfo[] diSrcDirectories = sourceDir.GetDirectories();
            foreach (DirectoryInfo diSrcDirectory in diSrcDirectories)
            {
                CopyDirectory(diSrcDirectory.FullName, new DirectoryInfo(Path.Combine(destDir.FullName, diSrcDirectory.Name)).FullName);
            }
        }
        public static void CopyFile(string sourcePath, string destPath)
        {
            string nameFile = sourcePath.Split('\\').LastOrDefault();
            System.IO.File.Copy(sourcePath, destPath + "\\" + nameFile, true);
        }
        public static void CopyFile(string sourcePath, string destPath, string nameNewFile)
        {
            string extensionFile = sourcePath.Split('.').LastOrDefault();
            System.IO.File.Copy(sourcePath, destPath + "\\" + nameNewFile + "." + extensionFile, true);
        }

    }
    public class String
    {
        private static Random random = new Random();
        public static string RandomNumber(int length)
        {
            int max = 1;
            int min = 0;
            for (int i = 0; i < length; i++)
            {
                max *= 10;

            }
            min = max / 10;
            return new Random().Next(min, max).ToString();
        }
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
    public class Audio
    {
        System.Media.SoundPlayer player;
        public bool isPlaying;
        public Audio(string pathAudio)
        {
            isPlaying = false;
            player = new System.Media.SoundPlayer(pathAudio);
            player.Load();
        }
        public string Play(bool isPlay = true, bool loops = false)
        {
            isPlaying = true;
            string status = "0";
            // If isPlay == true => have audio, else is mute
            if (isPlay)
            {
                if (loops)
                    player.PlayLooping();
                else
                    player.Play();
                status = "1";
            }
            
            return status;
        }
       
        public void Stop()
        {
            isPlaying = false;
            player.Stop();
        }
    }
    public class Time
    {
        public static string Now(string format = "", bool changeCharacter = false)
        {

            string full = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt");
            //output: dd/mm/yyyy hh/mm/ss xx
            string result = "";
            if (format == "only time")
            {
                string[] date = full.Split(' ');

                result = date[1];
            }
            else
            {
                result = full;
            }
            //Đổi các kí tự đặc biết lại để có thể dùng để lưu tên file, forder không bị lỗi
            if (changeCharacter)
            {
                result = result.Replace('/', '_');
                result = result.Replace(':', '-');
            }



            return result;
        }
    }
    public class Icon
    {
        public static string Robot = ":|]";
    }
    
    
}
